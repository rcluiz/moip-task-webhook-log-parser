# README #

### What is this repository for? ###

The WebHookFileParser program reads log files containing information
about webhook sent to MOIP's clients and identifies the most requested URLs 
and all response status returned by clients' servers.

### How do I get set up? ###

This application is packaged with Maven. So, to build or package it is necessary 
to  have Maven properly installed and configured.

This application uses some features from Java 8, therefore, it is necessary that 
Java SE Runtime Environment 8 is properly installed and Java's environment variables are 
properly configured.

To test the application you just need to call the JAR file informing 3 arguments:

1 - directory containing log files to parse
2 - file name reggex pattern that identify all log files to be parsed in the given directory
3 - the number of webhooks to show (optional, default = 3)

Example.:

java -jar moip-log-parser-0.0.1-SNAPSHOT.jar ./input/ log.*.txt 5

### Who do I talk to? ###

* Rafael Canato Luiz (rcluiz) - rafael.luiz@gmail.com