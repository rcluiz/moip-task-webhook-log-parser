package br.com.moip.webhooks.log;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Utility class containing methods to convert a map into a String in a reversed order
 * and to regroup data sets
 *   
 * @author Rafael Canato Luiz
 * @since 2017-05-11
 */
public class Util {

	/**
	 * Convert a {@link Map} to a {@link String} limited to 
	 * the top "limit" elements in a reversed sorted order
	 * 
	 * @param m {@link Map} to be converted
	 * @param limit the number of top elements to show
	 * @return the resulting string
	 */
	public static String reversedSortedMapToString(Map<String, Long> m, int limit) {

		Map<String, Long> map = new HashMap<String, Long>();

		if (m != null) {
			m.entrySet().stream().sorted(Map.Entry.<String, Long>comparingByValue().reversed()).limit(limit)
					.forEach(x -> map.put(x.getKey(), x.getValue()));

		}

		return reversedSortedMapToString(map);

	}

	/**
	 * Convert a {@link Map} to a {@link String} in a reversed sorted order
	 * 
	 * @return the resulting string
	 */
	public static String reversedSortedMapToString(Map<String, Long> m) {

		StringBuilder result = new StringBuilder();

		if (m != null) {
			m.entrySet().stream().sorted(Map.Entry.<String, Long>comparingByValue().reversed()).forEach(entry -> result
					.append(entry.getKey()).append(" - ").append(entry.getValue().toString()).append("\n"));
		}

		return result.toString();

	}


	/**
	 * Re-summarizes the input data set adding all children values 
	 * re-mapping then by the first key
	 *  
	 * @param dataset a {@link Map} indexed by {@link String} containing another 
	 * {@link Map} indexed by {@link String} mapping to {@link Long} elements
	 * @return a {@link Map} indexed by {@link String} mapping to {@link Long} 
	 * elements which is the summarization of all its children
	 */
	public static Map<String, Long> sumarizeByFirstKey(Map<String, Map<String, Long>> dataset) {

		return dataset.entrySet().stream()
				.collect(Collectors
						/*Get all children values and adds them re-mapping to the parent key */
						.toMap(e -> e.getKey(), e -> e.getValue().values().stream().reduce(0l, (a, b) -> a + b)));
	}

	/**
	 * Re-summarizes the input data set adding all children values 
	 * re-mapping then by the second key
	 *  
	 * @param dataset a {@link Map} indexed by {@link String} containing another 
	 * {@link Map} indexed by {@link String} mapping to {@link Long} elements
	 * @return a {@link Map} indexed by {@link String} mapping to {@link Long} 
	 * elements which is the summarization of all its children
	 */
	public static Map<String, Long> sumarizeBySecondKey(Map<String, Map<String, Long>> dataset) {

		return dataset.entrySet().stream()
				/* Maps all children and maps then to a list */
				.map(v -> v.getValue()).collect(Collectors.toList()).stream()
				/* Reduces all elements with the same key adding its values */
				.map(Map::entrySet).flatMap(Collection::stream).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, Long::sum));
	}

}