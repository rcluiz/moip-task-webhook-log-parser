package br.com.moip.webhooks.log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Parses a log file line with a specific regular expression extracting
 * request_to and response_status values.
 *   
 * @author Rafael Canato Luiz
 * @since 2017-05-11
 */
public class WebHookLogLine {
	
	public static final String DATA_ERROR;
	private static final String PATTERN;	
	private static final Pattern pattern;	
	static {
		DATA_ERROR = "#ERROR##ERROR##ERROR##ERROR#";
		PATTERN = "^.*" +  
	            "level=(?:.*) " + 
	            "response_body=\"(?:[^\"]*)\" " + 
	            "request_to=\"([^\"]*)\" " + 
	            "response_headers=(?:.*) " + 
	            "response_status=\"([^\"]*)\"" + 
	            ".*$";
		
		pattern = Pattern.compile(PATTERN);
	}	

	private String requestTo;

	private String responseStatus;

	/**
	 * Constructs a new object extracting values from log file line. If the line do not match
	 * the appropriate regular expression, all attributes are ste {@link #DATA_ERROR}
	 * 
	 * @param l log file line
	 */
	public WebHookLogLine(String l) {

		Matcher matcher = pattern.matcher(l);
		if (matcher.find()) {
			requestTo = matcher.group(1);
			responseStatus = matcher.group(2);
		} else {
			requestTo = DATA_ERROR;
			responseStatus = DATA_ERROR;
		}
	}

	public String getRequestTo() {
		return this.requestTo;
	}

	public String getResponseStatus() {
		return this.responseStatus;
	}

}
