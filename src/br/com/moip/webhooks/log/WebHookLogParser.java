package br.com.moip.webhooks.log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.net.URI;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * The WebHookLogParser program reads all files in given directory that matches
 * a regular expression and shows the most invoked URL with its quantities and
 * the response status and its quantities to the standard output.
 *   
 * @author Rafael Canato Luiz
 * @since 2017-05-11
 */
public class WebHookLogParser {

	/**
	 * 
	 */
	private CharsetDecoder dec;


	/**
	 * Initializes CharsetDecoder to ignore unparsed characters. 
	 */
	public WebHookLogParser() {
		this.dec = StandardCharsets.UTF_8.newDecoder().onMalformedInput(CodingErrorAction.IGNORE);
	}

	/**
	 * Gets a path list representing all files form dir that matches a given
	 * filename pattern.
	 *  
	 * @param dir directory to be searched for files 
	 * @param fileNamePattern filename pattern to be matched
	 * @return path list that matches a given criteria
	 */
	public static List<Path> getFilesToParse(String dir, String fileNamePattern) {
		List<Path> fileList = null;

		try {
			fileList = Files.list(Paths.get(dir)).filter(Files::isRegularFile)
					.filter(l -> l.getFileName().toString().matches(fileNamePattern)).collect(Collectors.toList());
		} catch (IOException e1) {
			e1.printStackTrace();
			fileList = new ArrayList<Path>();
		}

		return fileList != null ? fileList : new ArrayList<Path>();

	}

	/**
	 * Reads each line of a given file and parses it as an object of 
	 * type {@code WebHookLogLine}
	 * 
	 * @param URI representing the file to be read
	 * @return a {@code Map} indexed by "request_to" containing another
	 * {@code Map} indexed by status containing the total of each "response_status" by 
	 * each "request_to"
	 */
	public Map<String, Map<String, Long>> readFile(URI file) {

		/*
		 * Try-with-resources to close  Reader and BufferedReader
		 * used resources after block execution 
		 */
		Map<String, Map<String, Long>> dataset = null;
		try (Reader reader = Channels.newReader(FileChannel.open(Paths.get(file)), this.dec, -1);
				BufferedReader bufReader = new BufferedReader(reader);
				Stream<String> stream = bufReader.lines()) {
			
			/* 
			 * The resulting data structure is: request_to.response_status.count
			 */			
			dataset = stream
					/* For each file line creates a new WebHookLogLine object */
					.map(line -> {return new WebHookLogLine(line);})
					/* Group each line by "request_to" and then by "response_status" */
					.collect(Collectors.groupingBy(WebHookLogLine::getRequestTo, Collectors.groupingBy(WebHookLogLine::getResponseStatus, Collectors.counting())));

			/* Remove unparseable elements*/
			dataset.remove(WebHookLogLine.DATA_ERROR);

		} catch (IOException e) {
			dataset = new HashMap<String, Map<String, Long>>();
			e.printStackTrace();
		}

		return dataset;
	}

	/**
	 * Calls method {@link #readFile} for each log file and and then merge all returned maps.
	 *   
	 * @param files List of path containing log file information
	 * @return a {@code Map} indexed by "request_to" containing another
	 * {@code Map} indexed by status containing the total of each "response_status" by 
	 * each "request_to" from all files.
	 */
	public Map<String, Map<String, Long>> readFiles(List<Path> files) {
		
		/* List to store the map of each log file processed */
		List<Map<String, Map<String, Long>>> mapLists = new ArrayList<>();

		/* reads the log file */
		files.forEach(file -> mapLists.add(readFile(file.toUri())));
		
		/* Regroup all maps from each element of the List in a single Map */
		Map<String, Map<String, Long>> dataStructure = mapLists.stream()
				.flatMap(list -> list.entrySet().stream()) /* Flat the map */
				.collect(Collectors
						/* Reduces to a new Map with the same key adding all the values from its children */
						.toMap(Map.Entry::getKey, Map.Entry::getValue, (map1, map2) -> {
							Map<String, Long> merge = new HashMap<String, Long>(map1);
					        map2.forEach((k, v) -> merge.merge(k, v, Long::sum));
					        return merge;
				        }));

		return dataStructure;
	}

	/**
	 * Main method
	 * 
	 * @param args directory containing log files, 
	 * filename regex pattern to identify log files 
	 * and finally the number of webhooks to show (optional, default = 3)
	 */
	public static void main(String[] args) {

		if (args.length < 2 || args.length > 3) {
			System.out.println(
					"Usage: java -jar LIB.jar \"log file path\" \"filename regex pattern\" \"number of webhooks to show (optional, default = 3)\" ");
			System.exit(1);
		}

		String directory = args[0];
		String fileNamePattern = args[1];
		int topRequestToLimit = args.length == 3 ? Integer.valueOf(args[2]) : 3;

		List<Path> filesToParse = getFilesToParse(directory, fileNamePattern);

		if (!filesToParse.isEmpty()) {
			WebHookLogParser whfp = new WebHookLogParser();
			
			/* Parsed all log files and obtain a valid data set */
			Map<String, Map<String, Long>> dataset = whfp.readFiles(filesToParse);
			
			/* Regroup the data set indexed by "request_to" and "response_status" to be indexed just by "request_to" */
			Map<String, Long> requestToCount = Util.sumarizeByFirstKey(dataset);
			
			/* Regroup the data set indexed by "request_to" and "response_status" to be indexed just by "response_status" */
			Map<String, Long> responseStatusCount = Util.sumarizeBySecondKey(dataset);

			
			/* Print the result to stdout */
			System.out.println(Util.reversedSortedMapToString(requestToCount, topRequestToLimit));
			System.out.println(Util.reversedSortedMapToString(responseStatusCount));

		}
	}
}
